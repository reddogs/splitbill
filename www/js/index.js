$(document).ready(function() { //DOM Ready
    keepCornersSquare();
});
$(window).resize(function() {
    keepCornersSquare();
});

/*
 * set widtha and height parameters to make a div element square
 */
function keepCornersSquare() {
    var height5Prc = $(window).height() / 20;
    var width5Prc = $(window).width() / 20;
    var size;
    if (width5Prc > height5Prc) {
        size = width5Prc;
    } else {
        size = height5Prc;
    }
    $(".tl_corner, .bl_corner, .br_corner, .tr_corner").width(size);
    $(".tl_corner, .bl_corner, .br_corner, .tr_corner, .footer, .header").height(size);
    $(".left_column, .right_column, .content").css("top", size + "px");
    $(".content").css("left", size + "px");
}